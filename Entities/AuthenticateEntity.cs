﻿using System.ComponentModel.DataAnnotations;

namespace Gateway.SystemIdentity.Entities
{
    /// <summary>
    /// Login api request
    /// </summary>
    public class AuthenticateLoginRequest: RequestBase
    {
        /// <summary>
        /// Username
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        public string Username { get; set; }
        /// <summary>
        /// Password Base64(Compute(username + password))
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        public string Password { get; set; }
        /// <summary>
        /// Compute (HEX(username + password + ts + access_code))
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        public override string Sign { get; set; }
        public override string ServerExpectedSign() => $"{Username}{Password}{Ts}";
    }
    /// <summary>
    /// Refresh token api request
    /// </summary>
    public class AuthenticateRefreshTokenRequest : RequestBase
    {
        /// <summary>
        /// Refresh token code
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        public string Refresh { get; set; }
        /// <summary>
        /// Compute (HEX(refresh + ts + access_code))
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        public override string Sign { get; set; }
        public override string ServerExpectedSign() => $"{Refresh}{Ts}";
    }
    /// <summary>
    /// Verify token api request
    /// </summary>
    public class AuthenticateVerifyTokenRequest : RequestBase
    {
        /// <summary>
        /// Refresh token code
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        public string Refresh { get; set; }
        /// <summary>
        /// Compute (HEX(refresh + ts + access_code))
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        public override string Sign { get; set; }
        public override string ServerExpectedSign() => $"{Refresh}{Ts}";
    }
    /// <summary>
    /// 
    /// </summary>
    public class AuthenticateSignOutApiRequest : RequestBase
    {
        /// <summary>
        /// Refresh token code
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        public string Refresh { get; set; }
        /// <summary>
        /// Compute (HEX(refresh + ts + access_code))
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        public override string Sign { get; set; }
        public override string ServerExpectedSign() => $"{Refresh}{Ts}";
    }
    public class ProfileApiRequest : RequestBase
    {
        /// <summary>
        /// Refresh token code
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        public string Refresh { get; set; }
        /// <summary>
        /// Compute (HEX(refresh + ts + access_code))
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        public override string Sign { get; set; }
        public override string ServerExpectedSign() => $"{Refresh}{Ts}";
    }
    /// <summary>
    /// Authorize token api request
    /// </summary>
    public class AuthenticateAuthorizeTokenRequest : RequestBase
    {
        /// <summary>
        /// Refresh token code
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        public string Refresh { get; set; }
        /// <summary>
        /// Compute (HEX(refresh + ts + access_code))
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        public override string Sign { get; set; }
        public override string ServerExpectedSign() => $"{Refresh}{Ts}";
    }
}
