﻿using System.Collections.Generic;

namespace Gateway.SystemIdentity.Entities
{
    public class AppSettingEntity
    {
        public Dictionary<string, Detail> RpcAddress { get; set; }
        public JwtEntity Jwt { get; set; }

        public class Detail
        {
            public string Address { get; set; }
            public string Credential { get; set; }
        }

        public class JwtEntity
        {
            public string Audience { get; set; }
            public string Issuer { get; set; }
            public string SecureKey { get; set; }
        }
    }

}
