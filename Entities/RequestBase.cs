﻿using System.ComponentModel.DataAnnotations;
using Gateway.SystemIdentity.Utils;
using Gateway.SystemIdentity.Utils.Extensions;

namespace Gateway.SystemIdentity.Entities
{
    /// <summary>
    /// Api request base
    /// </summary>
    public abstract class RequestBase
    {
        /// <summary>
        /// Verify sign
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        public virtual string Sign { get; set; }
        /// <summary>
        /// Timestamp in millisecond
        /// </summary>
        public long Ts { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual string ServerExpectedSign() => "";

        /// <summary>
        /// Verify request
        /// </summary>
        /// <returns></returns>
        public virtual bool IsVerify(string header) =>
            string.IsNullOrEmpty(ServerExpectedSign()) || $"{ServerExpectedSign()}{header}".CalculateHash() == Sign;
    }
}
