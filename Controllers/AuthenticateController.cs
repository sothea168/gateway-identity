﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Gateway.SystemIdentity.Entities;
using Gateway.SystemIdentity.Utils;
using Gateway.SystemIdentity.Utils.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Service.Identity;

namespace Gateway.SystemIdentity.Controllers
{
    [Route("api/v1/[controller]/[action]")]
    [ApiController]
    public class AuthenticateController: ControllerBase
    {
        private readonly ILogger<AuthenticateController> _logger;
        private readonly HttpContext _httpContext;
        private readonly User.UserClient _client;
        public AuthenticateController(IHttpContextAccessor httpContextAccessor)
        {
            _httpContext = httpContextAccessor.HttpContext;
            _logger = _httpContext.GetService<ILogger<AuthenticateController>>();
            _client = _httpContext.GetService<User.UserClient>();
        }
        /// <summary>
        /// To generate access token
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost, ActionName("sign-in")]
        public async Task<object> TokenAsync([FromBody] AuthenticateLoginRequest request)
        {
            try
            {
                //TODO: fetch metadata
                var token = await _client.SignInAsync(new SignInRequest()
                {
                    ExpiredInMinute = 120,
                    Password = request.Password,
                    Platform = _httpContext.Request.Headers[HttpRequestHeaderConstant.Platform],
                    Username = request.Username,
                    Ip = Request.HttpContext.IpAddress()
                });

                return StatusCode(token.Code.GetStatusCode(), token);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "token");
                throw;
            }
        }
        /// <summary>
        /// To re-generate access token
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost, ActionName("refresh-token")]
        [Authorize]
        public async Task<object> RefreshTokenAsync([FromBody] AuthenticateRefreshTokenRequest request)
        {
            try
            {
                var refresh = await _client.RefreshCredentialAsync(new RefreshCredentialRequest
                {
                    Ip = _httpContext.IpAddress(),
                    AccessToken = _httpContext.GetAccessToken(),
                    RefreshToken = request.Refresh
                });
                return StatusCode(refresh.Code.GetStatusCode(), refresh);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "refresh-token");
                throw;
            }
        }
        /// <summary>
        /// To verify access token
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost, ActionName("verify-token")]
        [Authorize]
        public async Task<object> VerifyTokenAsync([FromBody] AuthenticateVerifyTokenRequest request)
        {
            try
            {
                var verify = await _client.VerifyCredentialAsync(new VerifyCredentialRequest
                {
                    RefreshToken = request.Refresh,
                    AccessToken = _httpContext.GetAccessToken(),
                    Ip = _httpContext.IpAddress()
                });
                return StatusCode(verify.Code.GetStatusCode(), verify);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "refresh-token");
                throw;
            }
        }
        /// <summary>
        /// Sign out current user
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost, ActionName("sign-out")]
        [Authorize]
        public async Task<object> SignOutAsync([FromBody] AuthenticateSignOutApiRequest request)
        {
            try
            {
                var signOut = await _client.SignOutAsync(new SignoutRequest()
                {
                    Ip = _httpContext.IpAddress(),
                    RefreshToken = request.Refresh,
                    AccessToken = _httpContext.GetAccessToken()
                });
                return StatusCode(signOut.Code.GetStatusCode(), signOut);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "sign-out-token");
                throw;
            }
        }
        /// <summary>
        /// Sign out current user
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost, ActionName("profile")]
        //[Authorize]
        public async Task<object> ProfileAsync([FromBody] ProfileApiRequest request)
        {
            try
            {
                var signOut = await _client.ProfileAsync(new ProfileRequest()
                {
                    Ip = _httpContext.IpAddress(),
                    RefreshToken = request.Refresh,
                    Platform = _httpContext.GetPlatform()
                });
                return signOut;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "refresh-token");
                throw;
            }
        }
    }
}
