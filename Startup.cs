using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Gateway.SystemIdentity.Entities;
using Gateway.SystemIdentity.Utils;
using Gateway.SystemIdentity.Utils.Extensions;
using Gateway.SystemIdentity.Utils.Filters;
using IdentityModel;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Service.Identity;
using Grpc.Core;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace Gateway.SystemIdentity
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration) => _configuration = configuration;
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            var appSettingsSection = _configuration.GetSection("AppSetting");
            var appSettings = appSettingsSection.Get<AppSettingEntity>();

            services.Configure<AppSettingEntity>(_configuration.GetSection("AppSetting"));
            services.AddControllers(options =>
            {
                options.Filters.Add<HttpResponseExceptionFilter>();
            }).AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.PropertyNamingPolicy = SnakeCaseNamingPolicyHelper.Instance;
                options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
            });
            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders = ForwardedHeaders.All;
            });
            services.Configure<FormOptions>(x =>
            {
                x.MultipartBodyLengthLimit = 10280000; // Limit on form body size
                x.MultipartHeadersLengthLimit = 10280000; // Limit on form header size
            });
            services.Configure<IISServerOptions>(options =>
            {
                options.MaxRequestBodySize = 837280000; // Limit on request body size
            });
            services.Configure<KestrelServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });
            //Build credential by system
            var callCredential = new Func<string, CallCredentials>((sys) =>
            {
                return CallCredentials.FromInterceptor((context, metadata) =>
                {
                    metadata.Add("Credential", appSettings.RpcAddress[sys].Credential);
                    return Task.CompletedTask;
                });
            });
            //Register rpc
            services.AddGrpcClient<User.UserClient>(o =>
            {
                o.Address = new Uri(appSettings.RpcAddress["Identity"].Address);
            })
            .ConfigureChannel(o =>
            {
                o.Credentials = ChannelCredentials.Create(new SslCredentials(), callCredential("Identity"));
            });
            services.AddHttpContextAccessor();
            //Jwt authorization
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(appSettings.Jwt.SecureKey));
                options.IncludeErrorDetails = true; // <- great for debugging
                options.SaveToken = true;
                // Configure the actual Bearer validation
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    IssuerSigningKey = securityKey,
                    ValidAudience = appSettings.Jwt.Audience,
                    ValidIssuer = appSettings.Jwt.Issuer,
                    RequireSignedTokens = true,
                    RequireExpirationTime = true, // <- JWTs are required to have "exp" property set
                    ValidateLifetime = true, // <- the "exp" will be validated
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero
                };
                options.Events = new JwtBearerEvents
                {
                    OnMessageReceived = context =>
                    {
                        const string con = "Receive";
                        var log = context.HttpContext.RequestServices.GetServices<ILogger<Startup>>().FirstOrDefault();
                        log.LogInformation($"======================={con}");
                        return Task.CompletedTask;
                    },
                    OnAuthenticationFailed = context => throw new LogicException(ExceptionType.Credential, MessageConstants.TokenExpired)
                };
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, ILogger<Startup> logger)
        {
            app.UseCors(c => c
                .AllowAnyOrigin()
                .AllowAnyHeader()
                .AllowAnyMethod());
            
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseMiddleware<MessageBrokerMiddleware>();
            
            app.Use(async (context, next) =>
            {
                try
                {
                    await next();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            });
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
