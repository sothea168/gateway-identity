﻿namespace Gateway.SystemIdentity.Utils
{
    /// <summary>
    /// Client provide header in http context
    /// </summary>
    public static class HeaderNames
    {
        public static string AccessCode = "Access-Code";
        public static string Platform = "Platform";
        public static string OwnerPlatform = "OwnerPlatform";
        public static string ForwardHeader = "X-Forwarded-For";
        public static string Bearer = "Authorization";
    }
}
