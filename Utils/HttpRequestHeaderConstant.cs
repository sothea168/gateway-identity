﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gateway.SystemIdentity.Utils
{
    public static class HttpRequestHeaderConstant
    {
        public const string Platform = "Platform";
        public const string Authorization = "Authorization";
    }
}
