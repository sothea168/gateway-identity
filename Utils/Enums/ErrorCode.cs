﻿using System.Net;

namespace Gateway.SystemIdentity.Utils.Enums
{
    /// <summary>
    /// 
    /// </summary>
    public enum ErrorCode
    {
        [StatusCode(StatusCode = HttpStatusCode.OK, 
            Codes = new[]
            {
                "OK"
            })]
        Ok,

        [StatusCode(StatusCode = HttpStatusCode.InternalServerError, 
            Codes = new[]
            {
                "SYS-ERR"
            })]
        SysError,

        [StatusCode(StatusCode = HttpStatusCode.Conflict,
            Codes = new[]
            {
                "SYS-EXIST", "DB-EXIST"
            })]
        Conflict,

        [StatusCode(StatusCode = HttpStatusCode.BadRequest,
            Codes = new[]
            {
                "REJECTED",
            })]
        BadRequest,

        [StatusCode(StatusCode = HttpStatusCode.NotFound,
            Codes = new[]
            {
                "SYS-NOT-FOUND", "DB-NOT-FOUND", "NOT-FOUND"
            })]
        NotFound,

        [StatusCode(StatusCode = HttpStatusCode.Unauthorized,
            Codes = new[]
            {
                "UNAUTHORIZED", "TOKEN-EXPIRED"
            })]
        Unauthorized
    }
}
