﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Gateway.SystemIdentity.Utils.Extensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Gateway.SystemIdentity.Utils
{
    public class SnakeCaseNamingPolicyHelper: JsonNamingPolicy
    {
        public static SnakeCaseNamingPolicyHelper Instance { get; } = new SnakeCaseNamingPolicyHelper();

        public static JsonSerializerSettings JsonSettingSnakeCase { get; } = new JsonSerializerSettings
            {ContractResolver = new DefaultContractResolver() {NamingStrategy = new SnakeCaseNamingStrategy()}};

        public override string ConvertName(string name) => name.ToSnakeCase();
    }
}
