﻿using System;

namespace Gateway.SystemIdentity.Utils
{
    /// <summary>
    /// 
    /// </summary>
    public enum ExceptionType : byte
    {
        /// <summary>
        /// 
        /// </summary>
        General = 0,
        /// <summary>
        /// 
        /// </summary>
        Logic = 1,
        /// <summary>
        /// 
        /// </summary>
        System = 2,
        /// <summary>
        /// 
        /// </summary>
        Credential = 3
    }
    /// <summary>
    /// 
    /// </summary>
    public class LogicException: Exception
    {
        /// <summary>
        /// 
        /// </summary>
        public ExceptionType ExceptionType { get; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="exceptionType"></param>
        /// <param name="message"></param>
        public LogicException(ExceptionType exceptionType, string message): base(message) => ExceptionType = exceptionType;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public LogicException(string message): this(ExceptionType.General, message) { }
    }
}
