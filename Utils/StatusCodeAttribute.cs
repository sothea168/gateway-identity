﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Gateway.SystemIdentity.Utils
{
    public class StatusCodeAttribute : System.Attribute
    {
        public HttpStatusCode StatusCode { get; set; }
        public string[] Codes { get; set; }
    }
}
