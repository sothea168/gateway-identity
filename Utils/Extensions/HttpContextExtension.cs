﻿using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Gateway.SystemIdentity.Utils.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class HttpContextExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetAccessCode(this HttpContext context) => context.Request.Headers[HeaderNames.AccessCode];
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetPlatform(this HttpContext context)
        {
            var platform = context.Request.Headers[HeaderNames.Platform];

            return string.IsNullOrEmpty(platform)
                ? context.RequestServices.GetService<IConfiguration>().GetSection(HeaderNames.OwnerPlatform).Get<string>()
                : platform.ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetUsername(this HttpContext context)
        {
            return !context.User.Identity.IsAuthenticated
                ? ""
                : Identity(context).Claims.First(c => c.Type == ClaimTypes.Name).Value;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string IpAddress(this HttpContext context)
        {
            if (context.Request.Headers.ContainsKey(HeaderNames.ForwardHeader))
                return context.Request.Headers[HeaderNames.ForwardHeader];
            else
                return context.Connection.RemoteIpAddress.MapToIPv4().ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private static ClaimsIdentity Identity(HttpContext context) => (ClaimsIdentity)context.User.Identity;

        public static TService GetService<TService>(this HttpContext httpContext) =>
            httpContext.RequestServices.GetServices<TService>().First();

        public static string GetAccessToken(this HttpContext context)
        {
            if (!context.Request.Headers.ContainsKey(HeaderNames.Bearer)) return null;
            {
                return context.Request.Headers[HeaderNames.Bearer].ToString().Split(" ")[1];
            }
        }
    }
}
