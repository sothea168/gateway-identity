﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using Gateway.SystemIdentity.Utils.Enums;
using Microsoft.OpenApi.Extensions;

namespace Gateway.SystemIdentity.Utils.Extensions
{
    public static class ErrorCodeExtension
    {
        public static int GetStatusCode(this string code)
        {
            try
            {
                var @enum = code.GetEnumFromDisplayNameEx<ErrorCode>();
                return (int)@enum.GetAttributeOfType<StatusCodeAttribute>().StatusCode;
            }
            catch (Exception)
            {
                return (int)HttpStatusCode.InternalServerError;
            }
        }

        public static T GetEnumFromDisplayNameEx<T>(this string displayName)
        {
            var type = typeof(T);
            if (!type.IsEnum)
            {
                return default(T);
            }

            foreach (var value in System.Enum.GetValues(type))
            {
                var field = type.GetField(value.ToString());
                
                var displayAttribute = (StatusCodeAttribute)field.GetCustomAttribute(typeof(StatusCodeAttribute));
                if (displayAttribute != null && (displayAttribute.Codes?.Any(x => x.ToUpper() == displayName.ToUpper()) ?? false))
                {
                    return (T)value;
                }
            }

            return default(T);
        }
    }
}
