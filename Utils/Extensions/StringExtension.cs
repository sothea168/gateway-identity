﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Gateway.SystemIdentity.Utils.Extensions
{
    /// <summary>
    /// String extension
    /// </summary>
    public static class StringExtension
    {
        /// <summary>
        /// Calculate hash from plain text in SHA512
        /// </summary>
        /// <param name="plain"></param>
        /// <returns></returns>
        public static string CalculateHash(this string plain)
        {
            using SHA512 sha = new SHA512CryptoServiceProvider();
            var bytes = Encoding.UTF8.GetBytes(plain);
            return BitConverter.ToString(sha.ComputeHash(bytes)).Replace("-", string.Empty);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ToSnakeCase(this string str) => string
            .Concat(str.Select((x, i) => i > 0 && char.IsUpper(x) ? "_" + x : x.ToString())).ToLower();
    }
}
