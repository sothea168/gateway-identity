﻿using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Gateway.SystemIdentity.Utils
{
    public class MessageBrokerMiddleware
    {
        private readonly ILogger<MessageBrokerMiddleware> _logger;
        private readonly RequestDelegate _next;
        public MessageBrokerMiddleware(RequestDelegate next, ILogger<MessageBrokerMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context, IConfiguration configuration)
        {
            if (!context.Items.ContainsKey("req_id"))
            {
                context.Items["req_id"] = $"{DateTimeOffset.UtcNow.ToUnixTimeMilliseconds()}";
            }
            try
            {
                await _next(context);
            }
            catch (Exception e)
            {
                if (e is LogicException le)
                {
                    var res = new
                    {
                        Code = "REJECTED",
                        Message = le.Message
                    };
                    var bytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(res, SnakeCaseNamingPolicyHelper.JsonSettingSnakeCase));
                    context.Response.StatusCode = 400;
                    context.Response.ContentType = "application/json";
                    await context.Response.Body.WriteAsync(bytes, 0, bytes.Length);
                }
                _logger.LogError(e, "Error");
            }
        }

        private static void Reject(ExceptionType type, string message) => throw new LogicException(type, message);
    }
}
