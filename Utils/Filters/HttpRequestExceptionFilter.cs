﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Gateway.SystemIdentity.Entities;
using Gateway.SystemIdentity.Utils.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Gateway.SystemIdentity.Utils.Filters
{
    public class HttpResponseExceptionFilter : IActionFilter, IOrderedFilter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public void OnActionExecuting(ActionExecutingContext context)
        {
            // validate sign
            var req = context.ActionArguments.FirstOrDefault().Value;
            if (!(req is RequestBase requestBase)) return;
            var additional = $"{context.HttpContext.GetAccessCode()}";
            // if (!requestBase.IsVerify(additional))
            //     context.Result = new ObjectResult(new
            //     {
            //         Code = "INVALID-SIGN",
            //         Message = "Sign is invalid"
            //     })
            //     {
            //         StatusCode = 400
            //     };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public void OnActionExecuted(ActionExecutedContext context)
        {
            switch (context.Exception)
            {
                case null:
                    return;
                
            }

            context.ExceptionHandled = true;
        }

        public int Order => 1;
    }
}
